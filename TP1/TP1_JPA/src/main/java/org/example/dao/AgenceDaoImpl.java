package org.example.dao;

import org.example.domain.Agence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;

public class AgenceDaoImpl implements IDaoAgence{

    public EntityManagerFactory emf = Persistence.createEntityManagerFactory("banque_pu");

    @Override
    public Agence createAgence(Agence agence) {
        try{
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(agence);

            em.getTransaction().commit();
            em.close();

            return (agence);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Agence findAgenceById(Long id) {
        try{
            EntityManager em = emf.createEntityManager();

            Agence agence = em.find(Agence.class, id);

            em.close();

            return (agence);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
