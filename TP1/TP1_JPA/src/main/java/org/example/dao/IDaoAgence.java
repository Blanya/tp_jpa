package org.example.dao;

import org.example.domain.Agence;

public interface IDaoAgence {
    Agence createAgence(Agence agence);

    Agence findAgenceById(Long id);
}
