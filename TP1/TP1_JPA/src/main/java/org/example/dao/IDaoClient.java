package org.example.dao;

import org.example.domain.Client;

public interface IDaoClient {
    Client createClient(Client client);

    Client getClient(Long id);
}
