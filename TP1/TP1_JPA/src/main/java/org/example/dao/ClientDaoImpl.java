package org.example.dao;

import org.example.domain.Client;
import org.example.domain.Compte;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ClientDaoImpl implements IDaoClient{
    public EntityManagerFactory emf = Persistence.createEntityManagerFactory("banque_pu");
    @Override
    public Client createClient(Client client) {
        try{
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(client);

            em.getTransaction().commit();
            em.close();

            return (client);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Client getClient(Long id) {
        try{
            EntityManager em = emf.createEntityManager();

            Client client = em.find(Client.class, id);


            return (client);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
