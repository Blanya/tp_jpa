package org.example.dao;

import org.example.domain.Compte;

public interface IDaoCompte {
    Compte createCompte(Compte compte);

    Compte getCompteById(Long id);
}
