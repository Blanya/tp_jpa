package org.example.dao;

import org.example.domain.Compte;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CompteDaoImpl implements IDaoCompte{
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("banque_pu");
    @Override
    public Compte createCompte(Compte compte) {
        try{
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(compte);

            em.getTransaction().commit();
            em.close();

            return (compte);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Compte getCompteById(Long id) {
        try{
            EntityManager em = emf.createEntityManager();

            Compte compte = em.find(Compte.class, id);


            return (compte);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
