package org.example.domain;

import org.example.dao.*;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Menu {

    public static EntityManagerFactory emf = Persistence.createEntityManagerFactory("banque_pu");
    public static IDaoAgence iDaoAgence = new AgenceDaoImpl();
    public static IDaoClient iDaoClient = new ClientDaoImpl();

    public static IDaoCompte iDaoCompte = new CompteDaoImpl();

    public static void printMenu()
    {
        String[] propositions = {"1. Créer une banque", "2. Créer un client", "3. Créer un compte", "0. Quitter"};

        for (String p: propositions ) {
            System.out.println(p);
        }

        Scanner sc = new Scanner(System.in);
        int choice = 0;

        try
        {
            choice = sc.nextInt();
        }catch(InputMismatchException e)
        {
            System.out.println("Erreur: Veuillez entrez un nombre");
            printMenu();
        }

        switch (choice)
        {
            case 1:
                System.out.println("Création d'une agence");
                createAgence();
                printMenu();
                break;
            case 2:
                System.out.println("Création d'un client");
                createClient();
                printMenu();
                break;
            case 3:
                System.out.println("Création d'un compte");
                Compte compte = createCompte();
                createCompteAdd(compte);
                printMenu();
            case 0:
                System.out.println("Au revoir!");
                emf.close();
                break;
            default:
                System.out.println("Commande invalide, veuillez réessayer");
                printMenu();
        }
    }

    public static void createAgence()
    {
        Scanner sc = new Scanner(System.in);

        try{
            System.out.println("Numéro et nom de la rue: ");
            String rue = sc.nextLine();
            System.out.println("Code postal: ");
            String zip = sc.next();
            System.out.println("Ville: ");
            String ville = sc.next();

            //get values from scanner
            Agence agence = new Agence(rue, zip, ville);

            // result > 0 if executed in db
            Agence agenceReturn = iDaoAgence.createAgence(agence);

            if(agenceReturn == null){
                System.out.println("Erreur lors de la création de l'agence");
            }else{
                System.out.println("Agence créée! ");
            }
        }catch (InputMismatchException ex){
            System.out.println("Données incorrectes, veuillez réessayer.");
            createAgence();
        }
    }

    public static void createClient()
    {
        Scanner sc = new Scanner(System.in);

        try {
            System.out.println("Nom du client: ");
            String nom = sc.next();
            System.out.println("Prenom: ");
            String prenom = sc.next();
            System.out.println("Date de naissance (format JJ/MM/AAAA): ");
            String date = sc.next();

            //recup date
            Date dateNaissance = null;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            //check good format => 0<m<=12
            simpleDateFormat.setLenient(false);

            dateNaissance = simpleDateFormat.parse(date);

            //link avec compte
            int relierCompte = 0;
            try {
                System.out.println("Souhaitez-vous relier un compte ? 1. Oui, 2. Non");
                relierCompte = sc.nextInt();
            } catch (InputMismatchException ex) {
                System.out.println("Erreur, veuillez recommencer svp");
                createClient();
            }

            //get values from scanner
            Client client = new Client();
            client.setNom(nom);
            client.setPrenom(prenom);
            client.setDateDeNaissance(dateNaissance);

            int creationCompte = 0;

            if (relierCompte == 2) {
                Client clientReturn = iDaoClient.createClient(client);

                if (clientReturn == null) {
                    System.out.println("Erreur lors de la création du client");
                } else {
                    System.out.println("Client ajouté! ");
                }
            } else if (relierCompte == 1) {
                creationCompte = 0;

                try {
                        System.out.println("Souhaitez-vous créer un compte ou relier un compte existant ? 1. Créer, 2. Relier");
                        creationCompte = sc.nextInt();
                } catch (InputMismatchException ex) {
                    System.out.println("Veuillez recommencer svp");
                    createClient();
                }
            }

            //creationCompte
            if (creationCompte == 1) {
                Compte compte = createCompte();
                client.addCompte(compte);

                Compte compteReturn = iDaoCompte.createCompte(compte);
                Client clientReturn = iDaoClient.createClient(client);

                if(clientReturn == null){
                    System.out.println("Erreur lors de la création du client");
                }else{
                        System.out.println("Client créé! ");

                    }

            } else if (creationCompte == 2) {
                System.out.println("Quel est le numéro du compte à ajouter au client? ");
                Long idCompte = sc.nextLong();

                Compte compte = iDaoCompte.getCompteById(idCompte);

                if (compte == null) {
                    System.out.println("Le compte n'existe pas, veuillez le créer");
                    createClient();
                } else {
                    List<Compte> comptes = client.getComptes();
                    comptes.add(compte);
                    client.setComptes(comptes);

                    Client clientReturn = iDaoClient.createClient(client);

                    if (clientReturn != null) {
                        System.out.println("Client créé! ");

                    } else {
                        System.out.println("Erreur lors de la création du client");
                    }
                }
            }


        }catch (InputMismatchException ex){
            System.out.println("Données incorrectes, veuillez réessayer.");
            createClient();
        } catch (ParseException e) {
            System.out.println("Format de la date de naissance incorrect");
            createClient();
        }
    }

    public static Compte createCompte()
    {
        Scanner sc = new Scanner(System.in);
        Compte compteReturn = null;

        try{
            System.out.println("Libelle du compte: ");
            String libelle = sc.nextLine();
            System.out.println("IBAN: ");
            String iban = sc.next();
            System.out.println("Solde: ");
            Double solde = sc.nextDouble();
            System.out.println("Numero de l'agence");
            Long nAgence = sc.nextLong();

            //get values from scanner
            Agence agence = iDaoAgence.findAgenceById(nAgence);

            if(agence == null){
                System.out.println("L'agence n'existe pas, veuillez l'enregistrer avant de créer le compte");
                printMenu();
            }else{
                Compte compte = new Compte();
                compte.setAgence(agence);
                compte.setLibelle(libelle);
                compte.setIban(iban);
                compte.setSolde(solde);

               return compte;
            }
        }catch (InputMismatchException ex){
            System.out.println("Données incorrectes, veuillez réessayer.");
            createCompte();
        }
        return null;
    }

    public static void createCompteAdd(Compte compte)
    {
        Scanner sc = new Scanner(System.in);


        System.out.println("A qui associer le compte? 0. Ne pas associer, sinon entrez l'id du client");
         try{
             long choice = sc.nextLong();
             if(choice == 0)
             {
                 Compte compteReturn = iDaoCompte.createCompte(compte);

                 if(compteReturn == null){
                     System.out.println("Erreur lors de la création du compte");
                 }else{
                     System.out.println("Compte créé! ");
                 }
             }
             else
             {
                 Client client = iDaoClient.getClient(choice);

                 if(client == null)
                 {
                     System.out.println("Le client n'existe pas");
                 }
                 else
                 {

                     Compte compteReturn = iDaoCompte.createCompte(compte);

                     //update and merge infos
                    EntityManager em = emf.createEntityManager();
                    em.getTransaction().begin();
                     List<Compte> comptes = client.getComptes();
                     comptes.add(compte);
                     client.setComptes(comptes);
                     em.merge(client);
                     em.getTransaction().commit();
                    em.close();
                     System.out.println("Compte créé");
                 }
             }
         }catch (InputMismatchException e)
         {
             System.out.println("Format invalide");
         }
    }
}
