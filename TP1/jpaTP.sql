CREATE DATABASE banque_bdd;

USE banque_bdd;

CREATE TABLE agence(
	id INT PRIMARY KEY AUTO_INCREMENT,
    rue VARCHAR(200),
    code_postal VARCHAR(5),
    ville VARCHAR(100)
);

CREATE TABLE client(
	id INT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(50),
    prenom VARCHAR(50),
    date_de_naissance DATE
);

CREATE TABLE compte(
	id INT PRIMARY KEY AUTO_INCREMENT,
    libelle VARCHAR(100) NOT NULL,
    iban VARCHAR(27) NOT NULL,
    solde DECIMAL(10,2),
    agence_id INT,
    FOREIGN KEY(agence_id) REFERENCES agence(id)
);

CREATE TABLE client_compte(
	client_id INT NOT NULL REFERENCES client(id) ON UPDATE CASCADE,
	compte_id INT NOT NULL REFERENCES compte(id) ON UPDATE CASCADE,
    PRIMARY KEY (client_id, compte_id)
);

SELECT * FROM agence;
SELECT * FROM client;
SELECT * FROM compte;
SELECT * FROM client_compte;

DROP TABLE agence;
DROP TABLE compte;
DROP TABLE client_compte;
DROP TABLE client;