package org.example;

import org.example.dao.*;
import org.example.domain.*;
import org.example.impl.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws ParseException {
//        persistData();
        iDaoAuthor.deleteAuthor(13L);
    }

    static IDaoRegister iDaoRegister = new RegisterDaoImpl();
    static IDaoCountry iDaoCountry = new CountryDaoImpl();
    static IDaoAuthor iDaoAuthor = new AuthorDaoImpl();
    static IDaoTypeBook iDaoTypeBook= new TypeBookDaoImpl();
    static IDaoBook iDaoBook = new BookDaoImpl();
    static IDaoCopy iDaoCopy = new CopyDaoImpl();
    static IDaoEdition iDaoEdition = new EditionDaoImpl();
    static IDaoCheck iDaoCheck = new CheckDaoImpl();



    public static void persistData() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

        //CREATE registers

        Register register = new Register("Mystere", "Martin", new java.sql.Date(simpleDateFormat.parse("17/02/1980").getTime()),"78 rue de la lys", "Croix", "78123", "0787654389", "m.mystere@gmail.com");

        Register register1 = new Register("Michel", "Jean",new java.sql.Date(simpleDateFormat.parse("09/12/1988").getTime()),"90 rue de l'hotel", "Béthune", "62400", "0765432114", "m.jean@hotmail.fr");

        iDaoRegister.createRegister(register);
        iDaoRegister.createRegister(register1);


        Country country = new Country("France");
        Country country1 = new Country("Portugal");
        Country country2 = new Country("Serbie");

        iDaoCountry.createCountry(country);
        iDaoCountry.createCountry(country1);
        iDaoCountry.createCountry(country2);


        //1er livre
        Author author = new Author("Lucet", "Lise", new java.sql.Date(simpleDateFormat.parse(" 12/09/1967").getTime()), country);

        iDaoAuthor.createAuthor(author);


        TypeBook typeBook = new TypeBook("Littérature");
        iDaoTypeBook.createTypeBook(typeBook);

        List<Author> authorList = new ArrayList<>();
        authorList.add(author);

        Book book = new Book("La vie c'est cool", "2009", "Un livre sur la vie. Elle est trop cool",
                typeBook, authorList);

        iDaoBook.createBook(book);

        List<Book> booksAuthor1 = new ArrayList<>();
        booksAuthor1.add(book);

        author.setBooks(booksAuthor1);

        iDaoAuthor.updateAuthor(author);

        Edition edition = new Edition("Editis");

        iDaoEdition.createEdition(edition);

        for (int i = 1; i <= 5 ; i++) {
            String ref = book.getId() + "" + i;
            Copy copy = new Copy(ref, book, edition);
            book.addCopy(copy);
//            iDaoCopy.createCopy(copy);
        }

        iDaoBook.updateBook(book);

        Copy copyl1 = iDaoCopy.findCopyById(book.getId() + "" + 1);
        Copy copyl2 = iDaoCopy.findCopyById(book.getId() + "" + 2);

        Check check1 = new Check(new java.sql.Date(simpleDateFormat.parse("08/04/2022").getTime()), 6, iDaoRegister.findRegisterById(2L),copyl1 );

        Check check2 = new Check(new java.sql.Date(simpleDateFormat.parse("09/01/2021").getTime()), 2, iDaoRegister.findRegisterById(2L), copyl2);

        copyl1.addCheck(check1);
        copyl2.addCheck(check2);

        iDaoCopy.updateCopy(copyl1);
        iDaoCopy.updateCopy(copyl2);

//        iDaoCheck.createCheck(check1);
//        iDaoCheck.createCheck(check2);

        //2eme livre

        TypeBook typeBook1 = new TypeBook("Voyage");
        iDaoTypeBook.createTypeBook(typeBook1);

        Book book1 = new Book("Là-bas, c'est loin", "2012", "Un livre sur le voyage en Asie",
                typeBook1);

//        iDaoBook.createBook(book1);

        List<Book> bookList = new ArrayList<>();
        bookList.add(book1);

        Author author1 = new Author("Deloin", "Alain", new java.sql.Date(simpleDateFormat.parse(" 12/08/1965").getTime()), country1, bookList);

        iDaoAuthor.createAuthor(author1);

        Edition edition1 = new Edition("Albin Michel");

        iDaoEdition.createEdition(edition1);

        for (int i = 1; i <= 9  ; i++) {
            String ref = book1.getId() + "" + i;
            Copy copy = new Copy(ref, book1, edition1);
//            iDaoCopy.createCopy(copy);
            book1.addCopy(copy);
        }

        iDaoBook.updateBook(book1);

        Copy copyl23 = iDaoCopy.findCopyById(book1.getId() + "" + 3);
        Copy copyl29 = iDaoCopy.findCopyById(book1.getId() + "" + 9);

        Check check3 = new Check(new java.sql.Date(simpleDateFormat.parse("01/02/2019").getTime()), 6, iDaoRegister.findRegisterById(2L), copyl23);

        Check check4 = new Check(new java.sql.Date(simpleDateFormat.parse("18/09/2021").getTime()), 1, iDaoRegister.findRegisterById(1L), copyl29);


        copyl23.addCheck(check3);
        copyl29.addCheck(check4);

        iDaoCopy.updateCopy(copyl23);
        iDaoCopy.updateCopy(copyl29);

//        iDaoCheck.createCheck(check3);
//        iDaoCheck.createCheck(check4);

        //3eme livre

        TypeBook typeBook2 = new TypeBook("Biographie");
        iDaoTypeBook.createTypeBook(typeBook2);

        Book book2 = new Book("L'histoire de ma vie'", "2022", "Un livre sur la vie de Mike Tom",
                typeBook2);

        List<Book> books = new ArrayList<>();
        books.add(book2);

        Author author2 = new Author("Tom", "Mike", new java.sql.Date(simpleDateFormat.parse(" 12/01/1956").getTime()), country2, books);

        iDaoAuthor.createAuthor(author2);

        Edition edition2 = new Edition("Hachette");

        iDaoEdition.createEdition(edition2);

        for (int i = 1; i <= 2 ; i++) {
            String ref = book2.getId() + "" + i;
            Copy copy = new Copy(ref, book2, edition2);
//            iDaoCopy.createCopy(copy);
            book2.addCopy(copy);
        }

        iDaoBook.updateBook(book2);

        Copy copyl31 = iDaoCopy.findCopyById(book2.getId() + "" + 1);

        Check check5 = new Check(new java.sql.Date(simpleDateFormat.parse("07/03/2021").getTime()), 3, iDaoRegister.findRegisterById(2L), copyl31);

        copyl31.addCheck(check5);

        iDaoCopy.updateCopy(copyl31);

//        iDaoCheck.createCheck(check5);
    }
}