package org.example.impl;

import org.example.dao.IDaoEdition;
import org.example.domain.Edition;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class EditionDaoImpl implements IDaoEdition {
    public EntityManagerFactory emf = Persistence.createEntityManagerFactory("book_pu");

    @Override
    public Edition createEdition(Edition edition) {
        try{
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(edition);

            em.getTransaction().commit();
            em.close();

            return (edition);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Edition findEditionById(Long id) {
        try{
            EntityManager em = emf.createEntityManager();

            Edition edition = em.find(Edition.class, id);

            em.close();

            return (edition);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Edition> getEdition() {
        try
        {
            EntityManager em = emf.createEntityManager();

            List<Edition> editions = em.createNativeQuery("SELECT * FROM Edition").getResultList();

            return editions;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean deleteEdition(Long id) {
        try {
            EntityManager em = emf.createEntityManager();
            em.createNativeQuery("DELETE FROM Edition WHERE id_ed = ?)").setParameter(1, id).executeUpdate();
            em.flush();

            em.close();
            return true;

        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Edition updateEdition(Edition edition) {
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(edition);
            em.getTransaction().commit();
            em.close();

            return edition;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
