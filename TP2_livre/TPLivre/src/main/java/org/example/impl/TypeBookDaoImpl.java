package org.example.impl;

import org.example.dao.IDaoTypeBook;
import org.example.domain.TypeBook;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class TypeBookDaoImpl implements IDaoTypeBook {
    public EntityManagerFactory emf = Persistence.createEntityManagerFactory("book_pu");

    @Override
    public TypeBook createTypeBook(TypeBook typeBook) {
        try{
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(typeBook);

            em.getTransaction().commit();
            em.close();

            return (typeBook);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public TypeBook findTypeBookById(Long id) {
        try{
            EntityManager em = emf.createEntityManager();

            TypeBook typeBook = em.find(TypeBook.class, id);

            em.close();

            return (typeBook);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<TypeBook> getTypeBook() {
        try
        {
            EntityManager em = emf.createEntityManager();

            List<TypeBook> typeBooks = em.createNativeQuery("SELECT * FROM Typelivre").getResultList();

            return typeBooks;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean deleteTypeBook(Long id) {
        try {
            EntityManager em = emf.createEntityManager();
            em.createNativeQuery("DELETE FROM Typelivre WHERE id_t = ?)").setParameter(1, id).executeUpdate();
            em.flush();

            em.close();
            return true;

        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public TypeBook updateTypeBook(TypeBook typeBook) {
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(typeBook);
            em.getTransaction().commit();
            em.close();

            return typeBook;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
