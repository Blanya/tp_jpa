package org.example.impl;

import org.example.dao.IDaoRegister;
import org.example.domain.Register;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class RegisterDaoImpl implements IDaoRegister {
    public EntityManagerFactory emf = Persistence.createEntityManagerFactory("book_pu");

    @Override
    public Register createRegister(Register register) {
        try{
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(register);

            em.getTransaction().commit();
            em.close();

            return (register);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Register findRegisterById(Long id) {
        try{
            EntityManager em = emf.createEntityManager();

            Register register = em.find(Register.class, id);

            em.close();

            return (register);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Register> getRegister() {
        try
        {
            EntityManager em = emf.createEntityManager();

            List<Register> registers = em.createNativeQuery("SELECT * FROM Inscrit").getResultList();

            return registers;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean deleteRegister(Long id) {
        try {
            EntityManager em = emf.createEntityManager();
            em.createNativeQuery("DELETE FROM Inscrit WHERE id_i = ?)").setParameter(1, id).executeUpdate();
            em.flush();

            em.close();
            return true;

        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Register updateRegister(Register register) {
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(register);
            em.getTransaction().commit();
            em.close();

            return register;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
