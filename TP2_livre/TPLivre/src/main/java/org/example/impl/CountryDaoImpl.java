package org.example.impl;

import org.example.dao.IDaoCountry;
import org.example.domain.Country;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class CountryDaoImpl implements IDaoCountry {
    public EntityManagerFactory emf = Persistence.createEntityManagerFactory("book_pu");

    @Override
    public Country createCountry(Country country) {
        try{
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(country);

            em.getTransaction().commit();
            em.close();

            return (country);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Country findCountryById(Long id) {
        try{
            EntityManager em = emf.createEntityManager();

            Country country = em.find(Country.class, id);

            em.close();

            return (country);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Country> getCountry() {
        try
        {
            EntityManager em = emf.createEntityManager();

            List<Country> countries = em.createNativeQuery("SELECT * FROM Pays").getResultList();

            return countries;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean deleteCountry(Long id) {
        try {
            EntityManager em = emf.createEntityManager();
            em.createNativeQuery("DELETE FROM Pays WHERE id_p = ?)").setParameter(1, id).executeUpdate();
            em.flush();

            em.close();
            return true;

        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Country updateCountry(Country country) {
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(country);
            em.getTransaction().commit();
            em.close();

            return country;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
