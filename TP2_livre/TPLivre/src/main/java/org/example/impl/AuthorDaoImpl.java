package org.example.impl;

import org.example.dao.IDaoAuthor;
import org.example.domain.Author;
import org.example.domain.Book;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.Serializable;
import java.util.List;

public class AuthorDaoImpl implements IDaoAuthor {

    public EntityManagerFactory emf = Persistence.createEntityManagerFactory("book_pu");

    @Override
    public Author createAuthor(Author author) {
        try{
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(author);

            em.getTransaction().commit();
            em.close();

            return (author);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Author findAuthorById(Long id) {
        try{
            EntityManager em = emf.createEntityManager();

            Author author = em.find(Author.class, id);

            em.close();

            return (author);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Author> getAuthor() {
        try
        {
            EntityManager em = emf.createEntityManager();

            List<Author> authors = em.createNativeQuery("SELECT * FROM Auteur").getResultList();

            return authors;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public boolean deleteAuthor(Long id) {
        try {
            EntityManager em = emf.createEntityManager();

            Author author = em.find(Author.class, id);

            em.getTransaction().begin();
//            supprimer que les livres avec un seul auteur
            for (Book b : author.getBooks()) {
                if (b.getAuthors().size() == 1) {
                    em.remove(b);
                } else {
                    b.getAuthors().remove(author);
                }
            }
            em.remove(author);
            em.getTransaction().commit();

            em.close();
            return true;

        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Author updateAuthor(Author author) {
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(author);
            em.getTransaction().commit();
            em.close();

            return author;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
