package org.example.impl;

import org.example.dao.IDaoBook;
import org.example.domain.Book;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class BookDaoImpl implements IDaoBook {
    public EntityManagerFactory emf = Persistence.createEntityManagerFactory("book_pu");

    @Override
    public Book createBook(Book book) {
        try{
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(book);

            em.getTransaction().commit();
            em.close();

            return (book);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Book findBookById(Long id) {
        try{
            EntityManager em = emf.createEntityManager();

            Book book = em.find(Book.class, id);

            em.close();

            return (book);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Book> getBook() {
        try
        {
            EntityManager em = emf.createEntityManager();

            List<Book> books = em.createNativeQuery("SELECT * FROM Livre").getResultList();

            return books;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean deleteBook(Long id) {
        try {
            EntityManager em = emf.createEntityManager();
            em.createNativeQuery("DELETE FROM Livre WHERE id_l = ?)").setParameter(1, id).executeUpdate();
            em.flush();

            em.close();
            return true;

        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Book updateBook(Book book) {
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(book);
            em.getTransaction().commit();
            em.close();

            return book;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
