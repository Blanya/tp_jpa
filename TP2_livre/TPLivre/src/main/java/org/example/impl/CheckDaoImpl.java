package org.example.impl;

import org.example.dao.IDaoCheck;
import org.example.domain.Check;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class CheckDaoImpl implements IDaoCheck {
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("book_pu");

    @Override
    public Check createCheck(Check check) {
        try{
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(check);

            em.getTransaction().commit();
            em.close();

            return (check);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Check findCheckById (Long id) {
        try{
            EntityManager em = emf.createEntityManager();

            Check check = em.find(Check.class, id);

            return (check);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Check> getCheck() {
        try
        {
            EntityManager em = emf.createEntityManager();

            List<Check> checks = em.createNativeQuery("SELECT * FROM Emprunt").getResultList();

            return checks;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean deleteCheck(Long id) {
        try {
            EntityManager em = emf.createEntityManager();
            em.createNativeQuery("DELETE FROM Emprunt WHERE id_em = ?)").setParameter(1, id).executeUpdate();
            em.flush();

            em.close();
            return true;

        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Check updateCheck(Check check) {
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(check);
            em.getTransaction().commit();
            em.close();

            return check;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
