package org.example.impl;

import org.example.dao.IDaoCopy;
import org.example.domain.Copy;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class CopyDaoImpl implements IDaoCopy {

    public EntityManagerFactory emf = Persistence.createEntityManagerFactory("book_pu");

    @Override
    public Copy createCopy(Copy copy) {
        try{
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.persist(copy);

            em.getTransaction().commit();
            em.close();

            return (copy);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Copy findCopyById(String id) {
        try{
            EntityManager em = emf.createEntityManager();

            Copy copy = em.find(Copy.class, id);

            em.close();

            return (copy);
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Copy> getCopy() {
        try
        {
            EntityManager em = emf.createEntityManager();

            List<Copy> copies = em.createNativeQuery("SELECT * FROM Exemplaire").getResultList();

            return copies;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean deleteCopy(String id) {
        try {
            EntityManager em = emf.createEntityManager();
            em.createNativeQuery("DELETE FROM Exemplaire WHERE id_e = ?)").setParameter(1, id).executeUpdate();
            em.flush();

            em.close();
            return true;

        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Copy updateCopy(Copy copy) {
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            em.merge(copy);
            em.getTransaction().commit();
            em.close();

            return copy;
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
