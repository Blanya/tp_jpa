package org.example.dao;

import org.example.domain.Author;

import java.util.List;

public interface IDaoAuthor {
    Author createAuthor(Author author);

    Author findAuthorById(Long id);

    List<Author> getAuthor();

    boolean deleteAuthor(Long id);

    Author updateAuthor(Author author);
}
