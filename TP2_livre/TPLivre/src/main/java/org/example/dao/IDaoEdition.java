package org.example.dao;

import org.example.domain.Edition;

import java.util.List;

public interface IDaoEdition {
    Edition createEdition(Edition edition);

    Edition findEditionById(Long id);

    List<Edition> getEdition();

    boolean deleteEdition(Long id);

    Edition updateEdition(Edition edition);
}
