package org.example.dao;

import org.example.domain.Book;

import java.util.List;

public interface IDaoBook {
    Book createBook(Book book);

    Book findBookById(Long id);

    List<Book> getBook();

    boolean deleteBook(Long id);

    Book updateBook(Book book);
}
