package org.example.dao;

import org.example.domain.Check;

import java.util.List;

public interface IDaoCheck {
    Check createCheck(Check check);

    Check findCheckById(Long id);

    List<Check> getCheck();

    boolean deleteCheck(Long id);

    Check updateCheck(Check check);
}
