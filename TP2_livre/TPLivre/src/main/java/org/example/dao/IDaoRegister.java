package org.example.dao;

import org.example.domain.Register;

import java.util.List;

public interface IDaoRegister {
    Register createRegister(Register register);

    Register findRegisterById(Long id);

    List<Register> getRegister();

    boolean deleteRegister(Long id);

    Register updateRegister(Register register);
}
