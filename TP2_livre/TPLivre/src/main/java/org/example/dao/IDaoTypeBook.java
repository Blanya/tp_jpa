package org.example.dao;

import org.example.domain.TypeBook;

import java.util.List;

public interface IDaoTypeBook {
    TypeBook createTypeBook(TypeBook typeBook);

    TypeBook findTypeBookById(Long id);

    List<TypeBook> getTypeBook();

    boolean deleteTypeBook(Long id);

    TypeBook updateTypeBook(TypeBook typeBook);
}
