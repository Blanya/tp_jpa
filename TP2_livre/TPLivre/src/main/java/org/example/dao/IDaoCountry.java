package org.example.dao;

import org.example.domain.Country;

import java.util.List;

public interface IDaoCountry {
    Country createCountry(Country countryy);

    Country findCountryById(Long id);

    List<Country> getCountry();

    boolean deleteCountry(Long id);

    Country updateCountry(Country countryy);
}
