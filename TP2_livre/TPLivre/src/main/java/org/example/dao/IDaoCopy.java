package org.example.dao;

import org.example.domain.Copy;

import java.util.List;

public interface IDaoCopy {
    Copy createCopy(Copy copy);

    Copy findCopyById(String id);

    List<Copy> getCopy();

    boolean deleteCopy(String id);

    Copy updateCopy(Copy copy);
}
