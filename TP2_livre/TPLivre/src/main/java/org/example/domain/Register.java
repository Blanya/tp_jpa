package org.example.domain;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "Inscrit")
public class Register {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_i")
    private Long id;

    @Column(name = "nom_i")
    private String lastname;

    @Column(name = "prenom_i")
    private String firstname;

    @Column(name = "date_naissance_i")
    private Date date;

    @Column(name = "rue_i")
    private String street;

    @Column(name = "ville_i")
    private String city;

    @Column(name = "cp_i", length = 5)
    private String postalCode;

    @Column(name = "tel_i",  length = 15)
    private String tel;

    @Column(name = "tel_portable_i",  length = 15)
    private String telPortable;

    @Column(name = "email_i")
    private String email;

    public Register() {
    }

    public Register(String lastname, String firstname, Date date, String street, String city, String postalCode, String telPortable, String email) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.date = date;
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
        this.telPortable = telPortable;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getTelPortable() {
        return telPortable;
    }

    public void setTelPortable(String telPortable) {
        this.telPortable = telPortable;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Register{" +
                "id=" + id +
                ", lastname='" + lastname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", date=" + date +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", tel='" + tel + '\'' +
                ", telPortable='" + telPortable + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
