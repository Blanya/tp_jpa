package org.example.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Emprunt")
public class Check {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_em")
    private Long id;

    @Column(name = "date_em")
    private Date dateCheck;

    @Column(name = "delais_em")
    private int delais;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "id_i")
    private Register register;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "ref_e")
    private Copy copy;

    public Check(Date dateCheck, int delais, Register register, Copy copy) {
        this.dateCheck = dateCheck;
        this.delais = delais;
        this.register = register;
        this.copy = copy;
    }

    public Check(){

    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateCheck() {
        return dateCheck;
    }

    public void setDateCheck(Date dateCheck) {
        this.dateCheck = dateCheck;
    }

    public int getDelais() {
        return delais;
    }

    public void setDelais(int delais) {
        this.delais = delais;
    }

    public Register getRegister() {
        return register;
    }

    public void setRegister(Register register) {
        this.register = register;
    }

    public Copy getCopy() {
        return copy;
    }

    public void setCopy(Copy copy) {
        this.copy = copy;
    }

    @Override
    public String toString() {
        return "Check{" +
                "id=" + id +
                ", dateCheck=" + dateCheck +
                ", delais=" + delais +
                ", register=" + register +
                ", copy=" + copy +
                '}';
    }
}
