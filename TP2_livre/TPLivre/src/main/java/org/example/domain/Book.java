package org.example.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Livre")
public class Book implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_l")
    private Long id;

    @Column(name = "titre_l")
    private String title;

    @Column(name = "annee_l")
    private String year;

    @Column(name = "resume_l")
    private String summary;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_t")
    private TypeBook typeBook;

    @ManyToMany(mappedBy = "books")
    private List<Author> authors;

    @OneToMany(
            mappedBy = "book",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Copy> copies = new ArrayList<>();

    public Book(String title, String year, String summary, TypeBook typeBook, List<Author> authors) {
        this.title = title;
        this.year = year;
        this.summary = summary;
        this.typeBook = typeBook;
        this.authors = authors;
    }

    public Book(String title, String year, String summary, TypeBook typeBook) {
        this.title = title;
        this.year = year;
        this.summary = summary;
        this.typeBook = typeBook;
    }

    public Book(){

    }

    public Book addCopy(Copy copy) {
        copies.add(copy);
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public TypeBook getTypeBook() {
        return typeBook;
    }

    public void setTypeBook(TypeBook typeBook) {
        this.typeBook = typeBook;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", year='" + year + '\'' +
                ", summary='" + summary + '\'' +
                ", typeBook=" + typeBook +
                ", authors=" + authors +
                '}';
    }
}
