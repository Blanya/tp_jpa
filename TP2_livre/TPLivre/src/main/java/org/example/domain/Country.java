package org.example.domain;

import javax.persistence.*;

@Entity
@Table(name = "Pays")
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_p")
    private Long id;

    @Column(name = "nom_p")
    private String name;

    public Country(String name) {
        this.name = name;
    }

    public Country(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Country{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
