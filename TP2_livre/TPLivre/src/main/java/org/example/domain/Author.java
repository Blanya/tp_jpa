package org.example.domain;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Auteur")
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_a")
    private Long id;

    @Column(name = "nom_a")
    private String lastname;

    @Column(name = "prenom_a")
    private String firstname;

    @Column(name = "date_naissance_a")
    private Date date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_p")
    private Country country;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "Redaction",
            joinColumns = @JoinColumn(name = "id_a"),
            inverseJoinColumns = @JoinColumn(name = "id_l"))
    private List<Book> books = new ArrayList<>();


    public Author(){

    }

    public Author(String lastname, String firstname, Date date, Country country, List<Book> books) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.date = date;
        this.country = country;
        this.books = books;
    }

    public Author(String lastname, String firstname, Date date, Country country) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.date = date;
        this.country = country;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", lastname='" + lastname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", date=" + date +
                ", country=" + country +
                '}';
    }
}
