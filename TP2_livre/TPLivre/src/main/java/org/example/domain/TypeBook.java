package org.example.domain;

import javax.persistence.*;

@Entity
@Table(name = "TypeLivre")
public class TypeBook {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_t")
    private Long id;

    @Column(name = "libelle_t")
    private String label;

    public TypeBook(String label) {
        this.label = label;
    }

    public TypeBook(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "TypeBook{" +
                "id=" + id +
                ", label='" + label + '\'' +
                '}';
    }
}
