package org.example.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Exemplaire")
public class Copy {
    @Id
    @Column(name = "ref_e")
    private String ref;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "id_l")
    private Book book;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "id_ed")
    private Edition edition;

    @OneToMany(
            fetch = FetchType.EAGER,
            mappedBy = "copy",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Check> checks = new ArrayList<>();

    public Copy(String ref, Book book, Edition edition) {
        this.ref = ref;
        this.book = book;
        this.edition = edition;
    }

    public Copy(String ref, Edition edition) {
        this.ref = ref;
        this.edition = edition;
    }

    public Copy(){

    }

    public Copy addCheck(Check check) {
        checks.add(check);
        return this;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Edition getEdition() {
        return edition;
    }

    public void setEdition(Edition edition) {
        this.edition = edition;
    }

    @Override
    public String toString() {
        return "Copy{" +
                "ref='" + ref + '\'' +
                ", book=" + book +
                ", edition=" + edition +
                '}';
    }
}
