package org.example.domain;

import javax.persistence.*;

@Entity
public class Edition {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_ed")
    private Long id;

    @Column(name = "nom_ed")
    private String name;

    public Edition(String name) {
        this.name = name;
    }

    public Edition(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Edition{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
